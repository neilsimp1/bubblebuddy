import events from 'events';
import onoff from 'onoff';
import IGpioAssignment from '@/main/table/IGpioAssignment';

export default class BubbleHockeyTable {
	public brandName: string = '';
	public model: string = '';

	private eventEmitter: events.EventEmitter;
	
	private buttonGpio: onoff.Gpio;
	private goalOneGpio: onoff.Gpio;
	private goalTwoGpio: onoff.Gpio;

	private buttonGpioTimer: number = -1;
	private goalGpioTimer: number = -1;
	private buttonGpioTimeout = 750;
	private goalGpioTimeout = 3000;

	public constructor(eventEmitter: events.EventEmitter, gpioAssignment: IGpioAssignment) {
		this.eventEmitter = eventEmitter;

		this.buttonGpio = new onoff.Gpio(gpioAssignment.buttonGpio, 'in', 'falling');
		this.goalOneGpio = new onoff.Gpio(gpioAssignment.goalOneGpio, 'in', 'falling');
		this.goalTwoGpio = new onoff.Gpio(gpioAssignment.goalTwoGpio, 'in', 'falling');

		this.buttonGpio.watch(_ => this.onButtonPush());
		this.goalOneGpio.watch(_ => this.onGoalScore(1));
		this.goalTwoGpio.watch(_ => this.onGoalScore(2));

		process.on('SIGINT', _ => this.dispose());
	}
	
	public onButtonPush(): void {
		const time = new Date().getTime();

		if (this.buttonGpioTimer + this.buttonGpioTimeout < time)
			this.buttonGpioTimer = -1;
		
		if (this.buttonGpioTimer === -1) {
			this.buttonGpioTimer = new Date().getTime();
			console.log('Button push');
			this.eventEmitter.emit('buttonPush');
		}
	}

	public onGoalScore(teamNum: number): void {
		const time = new Date().getTime();

		if (this.goalGpioTimer + this.goalGpioTimeout < time)
			this.goalGpioTimer = -1;
		
		if (this.goalGpioTimer === -1) {
			this.goalGpioTimer = new Date().getTime();
			console.log('Goal score: teamNum = ' + teamNum);
			this.eventEmitter.emit('goalScore', teamNum);
		}
	}

	private dispose(): void {
		this.eventEmitter.removeAllListeners();
		this.buttonGpio.unexport();
		this.goalOneGpio.unexport();
		this.goalTwoGpio.unexport();
	}
};
