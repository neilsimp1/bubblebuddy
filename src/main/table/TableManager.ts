import events from 'events';
import BubbleHockeyTable from '@/main/table/BubbleHockeyTable';
import IGpioAssignment from '@/main/table/IGpioAssignment';
import { BrowserWindow } from 'electron';

export default class TableManager {
	private bubbleHockeyTable: BubbleHockeyTable | null = null;
	private eventEmitter: events.EventEmitter;
	private gpioAssignment: IGpioAssignment;
	private window: BrowserWindow;

	public constructor(gpioAssignment: IGpioAssignment, window: BrowserWindow) {
		this.eventEmitter = new events.EventEmitter();
		this.gpioAssignment = gpioAssignment;
		this.window = window;
	}

	public initTable(): void {
		this.bubbleHockeyTable = new BubbleHockeyTable(this.eventEmitter, this.gpioAssignment);
		this.eventEmitter.on('buttonPush', _ => { this.onButtonPush() });
		this.eventEmitter.on('goalScore', teamNum => { this.onGoalScore(teamNum) });
	}

	private onButtonPush(): void {
		this.window.webContents.send('buttonPush');
	}

	private onGoalScore(teamNum: number): void {
		this.window.webContents.send('goalScore', teamNum);
	}
};