export default interface IGpioAssignment {
	buttonGpio: number;
	goalOneGpio: number;
	goalTwoGpio: number;
};
