export default interface IConfig {
	gpioAssignment: {
		buttonGpio: number,
		goalOneGpio: number,
		goalTwoGpio: number,
	};

	gpioEnabled: boolean,

	mediaRootPath: string;
	
	pics?: {
		ad: string[] | null;
		gameover: string[] | null;
		league: string[] | null;
		pause: string[] | null;
		team: string[] | null;
	};

	sounds?: {
		countdown: string[] | null;
		gameover: string[] | null;
		gameover_shutout: string[] | null;
		goal: string[] | null;
		period_1: string[] | null;
		period_2: string[] | null;
		period_3: string[] | null;
		period_4: string[] | null;
	};

	text?: {
		pause: string[] | null;
	};
};
