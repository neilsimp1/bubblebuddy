import electron from 'electron';
import fs from 'fs';
import path from 'path';
import IConfig from '@/main/config/IConfig';
import defaultConfig from './default.json';

export default class ConfigBuilder {
	public static buildConfig(isDevelopment: boolean): IConfig {
		const userDataPath = electron.app.getPath('userData');
		const configFilePath = path.join(userDataPath, 'config.json');
		
		let config: IConfig;

		if (!fs.existsSync(configFilePath)) {
			fs.writeFileSync(configFilePath, JSON.stringify(defaultConfig, null, 2));
			config = defaultConfig;
		}
		else {
			config = JSON.parse(fs.readFileSync(configFilePath).toString());
		}

		const mediaPaths: string[] = [];

		if (dirExists(config.mediaRootPath)) {
			const picPath = path.join(config.mediaRootPath, 'pic');
			const soundPath = path.join(config.mediaRootPath, 'sound');
			const textPath = path.join(config.mediaRootPath, 'text');
			if (dirExists(picPath)) ConfigBuilder.addPics(config, picPath);
			if (dirExists(soundPath)) ConfigBuilder.addSounds(config, soundPath);
			if (dirExists(textPath)) ConfigBuilder.addText(config, textPath);
		}

		return config;
	}

	private static addPics(config: IConfig, picPath: string) {
		//@ts-ignore
		if (config.pics === undefined) config.pics = {};

		const dirs = getDirectories(picPath);
		dirs.forEach(dirName => {
			if ((config.pics as any)[dirName] === undefined) (config.pics as any)[dirName] = [];
			let files = getFiles(path.join(picPath, dirName))
				.filter(x => isImage(x))
				.map(x => path.join(picPath, dirName, x))
				.map(x => 'file://' + x);
			(config.pics as any)[dirName] = (config.pics as any)[dirName].concat(files);
		});
	}

	private static addSounds(config: IConfig, soundPath: string) {
		//@ts-ignore
		if (config.sounds === undefined) config.sounds = {};

		const dirs = getDirectories(soundPath);
		dirs.forEach(dirName => {
			if ((config.sounds as any)[dirName] === undefined) (config.sounds as any)[dirName] = [];
			let files = getFiles(path.join(soundPath, dirName))
				.filter(x => isAudio(x))
				.map(x => path.join(soundPath, dirName, x))
				.map(x => 'file://' + x);
			(config.sounds as any)[dirName] = (config.sounds as any)[dirName].concat(files);
		});
	}

	private static addText(config: IConfig, textPath: string) {
		//@ts-ignore
		if (config.text === undefined) config.text = {};

		const files = getFiles(textPath).filter(x => isText(x));
		files.forEach(filename => {
			const groupName = filename.split('/').pop()!.split('.')[0];
			if ((config.text as any)[groupName] === undefined) (config.text as any)[groupName] = [];
			const text = fs.readFileSync(path.join(textPath, filename)).toString();
			(config.text as any)[groupName] = (config.text as any)[groupName].concat(text.split('\n').filter(x => x.trim() !== ''));
		});
	}
}

const dirExists = (rootPath: string) => fs.existsSync(rootPath) && fs.lstatSync(rootPath).isDirectory();
const getDirectories = (rootPath: string) => fs.readdirSync(rootPath, { withFileTypes: true }).filter(dirent => dirent.isDirectory()).map(dirent => dirent.name);
const getFiles = (rootPath: string) => fs.readdirSync(rootPath, { withFileTypes: true }).filter(dirent => dirent.isFile()).map(dirent => dirent.name);

const isFileType = (filename: string, exts: string[]) => exts.includes(filename.split('.').pop()!.toLowerCase());
const isAudio = (filename: string) => isFileType(filename, ['mp3']);
const isImage = (filename: string) => isFileType(filename, ['png', 'jpeg', 'jpg', 'gif', 'webp']);
const isText = (filename: string) => isFileType(filename, ['txt']);