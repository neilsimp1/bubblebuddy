import { exec } from 'child_process';
import { app, protocol, BrowserWindow, ipcMain } from 'electron';
import { createProtocol } from 'vue-cli-plugin-electron-builder/lib';
import installExtension, { VUEJS_DEVTOOLS } from 'electron-devtools-installer';
import IConfig from './config/IConfig';
import ConfigBuilder from '@/main/config/ConfigBuilder';
import TableManager from '@/main/table/TableManager';

let tableManager: TableManager | null = null;
const isDevelopment = process.env.NODE_ENV !== 'production';

protocol.registerSchemesAsPrivileged([ // Scheme must be registered before the app is ready
	{ scheme: 'app', privileges: { secure: true, standard: true, stream: true } }
]);

async function createWindow() {
	const config = ConfigBuilder.buildConfig(isDevelopment);
	
	const window = new BrowserWindow({
		autoHideMenuBar: !isDevelopment,
		fullscreen: true,
		webPreferences: {
			contextIsolation: false,
			nodeIntegration: true,
			webSecurity: false,
		},
	});

	bindIpcEvents(window, config);

	if (process.env.WEBPACK_DEV_SERVER_URL) {
		await window.loadURL(process.env.WEBPACK_DEV_SERVER_URL as string);
	}
	else {
		createProtocol('app');
		window.loadURL('app://./index.html');
	}

	if (config.gpioEnabled) {
		tableManager = new TableManager(config.gpioAssignment, window);
		tableManager.initTable();
	}
}

app.on('ready', async () => {	
	if (isDevelopment && !process.env.IS_TEST) {
		try {
			await installExtension(VUEJS_DEVTOOLS);
		}
		catch (e: any) {
			console.error('Vue Devtools failed to install:', e.toString());
		}
	}
	createWindow();
});

if (isDevelopment) {
	process.on('SIGTERM', app.quit); // Exit cleanly on request from parent process in development mode
}

const bindIpcEvents = (window: BrowserWindow, config: IConfig) => {
	ipcMain.on('exit', _ => { window.close() });
	ipcMain.on('shutdown', _ => {
		if (isDevelopment) {
			console.log('Shutting down lol jk');
		}
		else {
			exec('sudo poweroff');
		}
	});

	ipcMain.handle('getPics', _ => config.pics);
	ipcMain.handle('getSounds', _ => config.sounds);
	ipcMain.handle('getText', _ => config.text);
};
