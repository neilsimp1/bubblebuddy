import { Store } from 'vuex';
import { GameState } from '@/app/game/GameState';
import { mixins } from '@/app/mixins';
import SoundManager from '@/app/game/SoundManager';

export default class Game {
	private isPaused = false;
	private isSuddenDeath = false;
	private periodLength!: string;
	private periodLengthOvertime!: string;
	private overtimeMode!: number;
	private timer: NodeJS.Timeout | null = null;
	private soundManager: SoundManager;
	private $store: Store<any>

	public constructor($store: Store<any>, periodLength: string, periodLengthOvertime: string, overtimeIsSuddenDeath: boolean) {
		this.$store = $store;
		this.periodLength = periodLength;
		this.periodLengthOvertime = periodLengthOvertime;
		this.overtimeMode = overtimeIsSuddenDeath ? 1 : 0;
		this.soundManager = new SoundManager($store);
	}

	public start() {
		this.stop();

		this.$store!.commit('GameModule/gameState', GameState.STARTING);
		this.$store!.commit('GameModule/period', 0);
		this.$store!.commit('GameModule/teamOneScore', 0);
		this.$store!.commit('GameModule/teamTwoScore', 0);
		this.setRandomGameStartTimerMessage();

		this.soundManager.triggerSoundEvent('COUNTDOWN');

		setTimeout(() => {			
			this.$store!.commit('GameModule/gameState', GameState.RUNNING);
			this.$store!.commit('GameModule/period', 1);
			this.$store!.commit('GameModule/time', this.periodLength);
			this.timer = setInterval(((self) => () => { self.tick() })(this), 1000);
			this.soundManager.triggerSoundEvent('PERIOD_ONE');
		}, 3000);
	}

	public pause(pause: boolean = true) {
		this.isPaused = pause;
	}

	public stop() {
		clearTimeout(this.timer!);
	}

	public onGoalScore() {
		this.soundManager.triggerSoundEvent('GOAL');

		const pauseOnGoalPressResume = this.$store.getters['SystemModule/pauseOnGoalPressResume'];
		const pauseOnGoalTime = this.$store.getters['SystemModule/pauseOnGoalTime'];
		if (pauseOnGoalPressResume) {
			this.$store!.dispatch('GameModule/goalScorePause');
		}
		else if (pauseOnGoalTime > 0) {
			this.$store!.dispatch('GameModule/goalScorePause');
			setTimeout(() => {
				this.$store!.dispatch('GameModule/goalScoreUnpause');
			}, pauseOnGoalTime * 1000);
		}
	}

	private tick() {
		if (!this.isPaused) {
			const period = this.$store!.getters['GameModule/period'] as number;
			const time = this.$store!.getters['GameModule/time'] as string;
			const teamOneScore = this.$store!.getters['GameModule/teamOneScore'] as number;
			const teamTwoScore = this.$store!.getters['GameModule/teamTwoScore'] as number;
			
			// TODO: each period type could be a method, normalPeriod, finalPeriod, suddenDeathPeriod
			// Each could be added to an array or something and then we call that method for each tick
			if (this.isSuddenDeath) {
				if (teamOneScore !== teamTwoScore) { // Winner
					this.gameOver();
				}
			}
			else {
				if (time === '00:00') { // Period over
					if (period >= 3) { // End of period 3 or OT
						if (teamOneScore === teamTwoScore) { // Overtime
							if (this.overtimeMode === 0 && period != 4) { // Regular period OT
								this.$store!.commit('GameModule/period', period + 1);
								this.$store!.commit('GameModule/time', this.periodLengthOvertime);
							}
							else { // Sudden death OT
								if (period < 4) this.$store!.commit('GameModule/period', period + 1);
								this.$store!.commit('GameModule/time', 'XX:XX');
								this.isSuddenDeath = true;

								this.soundManager.triggerSoundEvent('SUDDEN_DEATH');
							}
						}
						else { // Winner
							this.gameOver();
						}
					}
					else { // End of other period
						this.$store!.commit('GameModule/period', period + 1);
						this.$store!.commit('GameModule/time', this.periodLength);

						if (period === 1) this.soundManager.triggerSoundEvent('PERIOD_TWO');
						if (period === 2) this.soundManager.triggerSoundEvent('PERIOD_THREE');
					}
				}
				else {
					this.$store!.commit('GameModule/time', this.getNextSecond());
				}
			}
		}
	}

	private setRandomGameStartTimerMessage(): void {
		const rand = Math.floor(Math.random() * 4) + 1;
		switch (rand) {
			case 1:
				this.$store!.commit('GameModule/time', 'BUBBLE');
				setTimeout(() => { this.$store!.commit('GameModule/time', 'HOCKEY') }, 1500);
				break;
			case 2:
				this.$store!.commit('GameModule/time', '3');
				setTimeout(() => { this.$store!.commit('GameModule/time', '3 2') }, 1000);
				setTimeout(() => { this.$store!.commit('GameModule/time', '3 2 1') }, 2000);
				setTimeout(() => { this.$store!.commit('GameModule/time', 'GO!!!') }, 3000);
				break;
			case 3:
				this.$store!.commit('GameModule/time', 'READY');
				setTimeout(() => { this.$store!.commit('GameModule/time', 'SPA-') }, 1000);
				setTimeout(() => { this.$store!.commit('GameModule/time', 'GHETTI') }, 2000);
				break;
			case 4:
				this.$store!.commit('GameModule/time', 'START');
				break;
		}
	}

	private getNextSecond(): string {
		const time = this.$store!.getters['GameModule/time'];
		let [minutes, seconds] = time.split(':').map((x: string) => parseInt(x));

		if (seconds > 0) {
			seconds--;
		}
		else {
			seconds = 59;
			minutes--;
		}
		
		return `${mixins.methods.padLeft(minutes, 2)}:${mixins.methods.padLeft(seconds, 2)}`;
	}

	private gameOver(): void {
		clearInterval(this.timer!);

		this.$store!.commit('GameModule/gameState', GameState.OVER);

		this.soundManager.triggerSoundEvent('GAME_OVER');
		if (this.$store!.getters['GameModule/teamOneScore'] === 0 || this.$store!.getters['GameModule/teamTwoScore'] === 0) {
			setTimeout(() => {
				this.soundManager.triggerSoundEvent('FLAWLESS_VICTORY');
			}, 2000);
		}
	}
}
