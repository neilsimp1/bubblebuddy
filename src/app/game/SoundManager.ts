import { Store } from 'vuex';
import ISoundCollection from '@/app/store/models/ISoundCollection';

export default class SoundManager {
	private $store: Store<any> | null = null;
	private htmlAudioElement = new Audio();	
	private internalSounds: ISoundCollection = {
		countdown: [
			'/assets/sound/countdown/countdown_01.mp3',
			'/assets/sound/countdown/countdown_02.mp3',
			'/assets/sound/countdown/countdown_03.mp3',
			'/assets/sound/countdown/countdown_04.mp3',
		],
		gameover: [
			'/assets/sound/gameover/goal_buzzer_01.mp3',
		],
		gameover_shutout: [],
		goal: [
			'/assets/sound/goal/goal_buzzer_01.mp3',
		],
		period_1: [],
		period_2: [],
		period_3: [],
		period_4: [],
	};

	private get externalSounds(): ISoundCollection { return this.$store!.getters['SystemModule/mediaSounds'] }
	private get sounds(): ISoundCollection {
		return {
			countdown: this.internalSounds.countdown.concat(this.externalSounds.countdown),
			gameover: this.internalSounds.gameover.concat(this.externalSounds.gameover),
			gameover_shutout: this.internalSounds.gameover_shutout.concat(this.externalSounds.gameover_shutout),
			goal: this.internalSounds.goal.concat(this.externalSounds.goal),
			period_1: this.internalSounds.period_1.concat(this.externalSounds.period_1),
			period_2: this.internalSounds.period_2.concat(this.externalSounds.period_2),
			period_3: this.internalSounds.period_3.concat(this.externalSounds.period_3),
			period_4: this.internalSounds.period_4.concat(this.externalSounds.period_4),
		};
	}

	public constructor($store: Store<any>) {
		this.$store = $store;
	}

	public triggerSoundEvent(eventName: string): void {
		if (!this.$store!.getters['SystemModule/soundOnOff']) return;

		let url = '';
		switch (eventName) {
			case 'COUNTDOWN':
				url = this.getRandomSound(this.sounds.countdown);
				break;
			case 'GOAL':
				url = this.getRandomSound(this.sounds.goal);
				break;
			case 'PERIOD_ONE':
				url = this.getRandomSound(this.sounds.period_1);
				break;
			case 'PERIOD_TWO':
				url = this.getRandomSound(this.sounds.period_2);
				break;
			case 'PERIOD_THREE':
				url = this.getRandomSound(this.sounds.period_3);
				break;
			case 'SUDDEN_DEATH':
				url = this.getRandomSound(this.sounds.period_4);
				break;
			case 'GAME_OVER':
				url = this.getRandomSound(this.sounds.gameover);
				break;			
			case 'FLAWLESS_VICTORY':
				url = this.getRandomSound(this.sounds.gameover_shutout);
				break;
		}

		if (!!url) new Audio(url).play();
	}

	private getRandomSound(urls: string[]): string {
		if (!urls.length) return null!;
		const rand = Math.floor(Math.random() * urls.length);
		return urls[rand];
	}
}
