import { createStore } from 'vuex';
import createPersistedState from 'vuex-persistedstate';
import GameModule from '@/app/store/modules/GameModule';
import SystemModule from '@/app/store/modules/SystemModule';

export default createStore({
	state: {
	},
	mutations: {
	},
	actions: {
	},
	modules: {
		GameModule,
		SystemModule,
	},
	plugins: [
		createPersistedState({
			paths: ['SystemModule'],
			storage: window.localStorage,
		}),
	],
})
