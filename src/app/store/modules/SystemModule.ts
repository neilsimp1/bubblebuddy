const { ipcRenderer } = window.require('electron');
import IPicCollection from '@/app/store/models/IPicCollection';
import ISoundCollection from '@/app/store/models/ISoundCollection';
import ITextCollection from '@/app/store/models/ITextCollection';
import IOption from '../models/IOption';

type State = any;
type Store = any;

const getFileFriendlyName = (url: string): string => url.split('/').pop()!.split('.')[0];

const defaultTeamOptions: IOption[] = [
	{ key: 'COLOR', value: 'Solid color', isDisabled: false },
	{ key: '', value: '-----------', isDisabled: true },
];

const state = {
	mediaPics: <IPicCollection | null> null,
	mediaSounds: <ISoundCollection | null> null,
	mediaText: <ITextCollection | null> null,
	overtimeMode: <boolean> true,
	pauseOnGoalPressResume: <boolean> true,
	pauseOnGoalTime: <number> 0,
	periodLength: <string> '02:00',
	periodLengthOvertime: <string> '01:30',
	soundOnOff: <boolean> true,
	teamOneColor: <string> '#112233',
	teamTwoColor: <string> '#ff8838',
	teamOneName: <string> 'Team #1',
	teamTwoName: <string> 'Team #2',
	teamOneLogoUrl: <string> 'COLOR',
	teamTwoLogoUrl: <string> 'COLOR',
};

const getters = {
	mediaPics: (state: State) => state.mediaPics,
	mediaSounds: (state: State) => state.mediaSounds,
	mediaText: (state: State) => state.mediaText,
	overtimeMode: (state: State) => state.overtimeMode,
	pauseOnGoalPressResume: (state: State) => state.pauseOnGoalPressResume,
	pauseOnGoalTime: (state: State) => state.pauseOnGoalTime,
	periodLength: (state: State) => state.periodLength,
	periodLengthOvertime: (state: State) => state.periodLengthOvertime,
	soundOnOff: (state: State) => state.soundOnOff,
	teamOneColor: (state: State) => state.teamOneColor,
	teamTwoColor: (state: State) => state.teamTwoColor,
	teamOneName: (state: State) => state.teamOneName,
	teamTwoName: (state: State) => state.teamTwoName,
	teamOneLogoUrl: (state: State) => state.teamOneLogoUrl,
	teamTwoLogoUrl: (state: State) => state.teamTwoLogoUrl,
	teamLogoOptions: (state: State) => {
		let options = defaultTeamOptions;
		if (!!state.mediaPics && state.mediaPics.team.length){
			options = options.concat(state.mediaPics.team.map((x: string) => { return { key: x, value: getFileFriendlyName(x), isDisabled: false }}));
		}

		return options;
	},
};

const mutations = {
	mediaPics: (state: State, value: number) => state.mediaPics = value,
	mediaSounds: (state: State, value: number) => state.mediaSounds = value,
	mediaText: (state: State, value: number) => state.mediaText = value,
	overtimeMode: (state: State, value: boolean) => state.overtimeMode = value,
	pauseOnGoalPressResume: (state: State, value: boolean) => state.pauseOnGoalPressResume = value,
	pauseOnGoalTime: (state: State, value: boolean) => state.pauseOnGoalTime = value,
	periodLength: (state: State, value: string) => state.periodLength = value,
	periodLengthOvertime: (state: State, value: string) => state.periodLengthOvertime = value,
	soundOnOff: (state: State, value: boolean) => state.soundOnOff = value,
	teamOneColor: (state: State, value: string) => state.teamOneColor = value,
	teamTwoColor: (state: State, value: string) => state.teamTwoColor = value,
	teamOneName: (state: State, value: string) => state.teamOneName = value,
	teamTwoName: (state: State, value: string) => state.teamTwoName = value,
	teamOneLogoUrl: (state: State, value: string) => {
		state.teamOneLogoUrl = value;
		if (value !== 'COLOR') state.teamOneName = getFileFriendlyName(value);
	},
	teamTwoLogoUrl: (state: State, value: string) => {
		state.teamTwoLogoUrl = value;
		if (value !== 'COLOR') state.teamTwoName = getFileFriendlyName(value);
	},
};

const actions = {
	exit() {
		ipcRenderer.sendSync('exit');
	},
	shutdown() {
		ipcRenderer.sendSync('shutdown');
	},
	async loadMedia(context: Store) {
		const pics = await ipcRenderer.invoke('getPics');
		const sounds = await ipcRenderer.invoke('getSounds');
		const text = await ipcRenderer.invoke('getText');
		context.commit('mediaPics', pics);
		context.commit('mediaSounds', sounds);
		context.commit('mediaText', text);
	},
};

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions,
};
