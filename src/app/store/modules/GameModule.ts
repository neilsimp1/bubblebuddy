const { ipcRenderer } = window.require('electron');
import Game from '@/app/game/Game';
import { GameState } from '@/app/game/GameState';

type State = any;
type Store = any;

let game: Game | null = null;

const state = {
	gameState: <GameState> GameState.NOT_STARTED,
	period: <number> 0,
	periodLength: <string> '00:00',
	periodLengthOvertime: <string> '00:00',
	time: <string> '00:00',
	teamOneScore: <number> 0,
	teamTwoScore: <number> 0,
};

const getters = {
	gameState: (state: State) => state.gameState,
	period: (state: State) => state.period,
	periodLength: (state: State) => state.periodLength,
	periodLengthOvertime: (state: State) => state.periodLengthOvertime,
	time: (state: State) => state.time,
	teamOneScore: (state: State) => state.teamOneScore,
	teamTwoScore: (state: State) => state.teamTwoScore,
};

const mutations = {
	gameState: (state: State, value: GameState) => state.gameState = value,
	period: (state: State, value: number) => state.period = value,
	periodLength: (state: State, value: number) => state.periodLength = value,
	periodLengthOvertime: (state: State, value: number) => state.periodLengthOvertime = value,
	time: (state: State, value: string) => state.time = value,
	teamOneScore: (state: State, value: number) => state.teamOneScore = value,
	teamTwoScore: (state: State, value: number) => state.teamTwoScore = value,
};

const actions = {
	startGame(this: Store, context: Store) {
		const gameState = context.getters['gameState'];
		if (gameState === GameState.NOT_STARTED || gameState === GameState.OVER) {
			if (game !== null) game.stop();
			context.commit('periodLength', this.getters['SystemModule/periodLength']);
			context.commit('periodLengthOvertime', this.getters['SystemModule/periodLengthOvertime']);
			game = new Game(this, context.getters['periodLength'], context.getters['periodLengthOvertime'], this.getters['SystemModule/overtimeMode']);
			game.start();
		}
	},
	restartGame(this: Store, context: Store) {
		const gameState = context.getters['gameState'];
		if (gameState !== GameState.STARTING) {
			if (game !== null) game.stop();
			context.commit('periodLength', this.getters['SystemModule/periodLength']);
			context.commit('periodLengthOvertime', this.getters['SystemModule/periodLengthOvertime']);
			game = new Game(this, context.getters['periodLength'], context.getters['periodLengthOvertime'], this.getters['SystemModule/overtimeMode']);
			game.start();
		}
	},
	buttonPush(context: Store) {
		const gameState = context.getters['gameState'];
		switch (gameState) {
			case GameState.NOT_STARTED:
			case GameState.OVER:
				context.dispatch('startGame');
				break;
			case GameState.STARTING:
				break;
			case GameState.RUNNING:
				context.commit('gameState', GameState.PAUSED);
				game?.pause();
				break;
			case GameState.GOALPAUSED:
				// context.commit('gameState', GameState.RUNNING);
				// game?.pause(false);
				// break;
			case GameState.PAUSED:
				context.commit('gameState', GameState.RUNNING);
				game?.pause(false);
				break;
		}
	},
	goalScore(context: Store, teamNum: number) {
		const gameState = context.getters['gameState'];
		if (gameState === GameState.RUNNING) {
			const propName = teamNum === 1 ? 'teamOneScore' : 'teamTwoScore';
			let newScore = context.getters[propName] + 1		
			context.commit(propName, newScore);
			game?.onGoalScore();
		}
	},
	goalScorePause(context: Store) {
		context.commit('gameState', GameState.GOALPAUSED);
		game?.pause();
	},
	goalScoreUnpause(context: Store) {
		context.commit('gameState', GameState.RUNNING);
		game?.pause(false);
	},
	settingsPause(context: Store) {
		context.commit('gameState', GameState.SETTINGSPAUSED);
		game?.pause();
	},
	settingsUnpause(context: Store) {
		context.commit('gameState', GameState.RUNNING);
		game?.pause(false);
	},
};

export default {
	namespaced: true,
	state,
	getters,
	mutations,
	actions,
};
