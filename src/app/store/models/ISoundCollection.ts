export default interface ISoundCollection {
	countdown: string[];
	gameover: string[];
	gameover_shutout: string[];
	goal: string[];
	period_1: string[];
	period_2: string[];
	period_3: string[];
	period_4: string[];
}
