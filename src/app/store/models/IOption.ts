export default interface IOption {
	key: string;
	value: string;
	isDisabled: boolean;
}
