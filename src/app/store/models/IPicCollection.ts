export default interface IPicCollection {
	ad: string[];
	gameover: string[];
	league: string[];
	pause: string[];
	team: string[];
}
