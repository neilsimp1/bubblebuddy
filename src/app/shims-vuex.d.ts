import { ComponentCustomProperties } from 'vue';
import { Store } from 'vuex';

declare module '@vue/runtime-core' {
	type State = any;
	interface ComponentCustomProperties {
		$store: Store<State>
	}
}
