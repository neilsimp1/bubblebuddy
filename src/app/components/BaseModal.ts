import { Options, Vue } from 'vue-class-component';

@Options({
	props: {
		isVisible: Boolean,
	}
})
export default class BaseModal extends Vue {
	protected isVisible!: boolean;
	protected name!: string;

	protected onModalBgClick(event: Event): void {
		if ((event?.target as HTMLElement).classList.contains('modal')) this.closeModal();
	}

	protected closeModal(): void {
		this.$emit('close' + this.name);
	}
}