export const mixins = {
	methods: {
		padLeft: (value: any, numDigits: number): string => {
			if (!value) value = '';		
			value = value.toString();
		
			const length = value.length;
		
			if (length >= numDigits) return value;
		
			const numZeros = numDigits - length;
			for (let i = 0; i < numZeros; i++) value = '0' + value;
			return value;
		}
	},
};