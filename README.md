# bubblebuddy
![project logo](./icon.png)

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

---

## Table setup
Currently, only a [Carrom Super Stick Hockey](https://carrom.com/product/super-stick-hockey/) table is supported. Other tables should be able to work with a little effort.
Using the default configuration, connect the following wires to the following [pins](https://www.etechnophiles.com/wp-content/uploads/2020/12/R-Pi-3-B-Pinout-768x572.jpg?ezimgfmt=ng:webp/ngcb40) on a Raspberry Pi 3B+:

```
Table
	GPIO 5		Black cable, this is for power
	GPIO 5(G)	This is the GND immediately next to GPIO 5
	GPIO 26		Green cable running to goal 2 sensor
	GPIO 27		Green cable running to goal 1 sensor
```

---

## Configuration
After first run, a file named `~/.config/bubblebuddy/config.json` will be created using the default settings.
This can be edited to change behavior on subsequent runs.

### mediaRootPath
Point to a directory containing images/text with the following subdirectory tree and files will be used randomly at runtime.
```
├── pic
│   ├── ad
│   ├── gameover
│   ├── league
│   ├── pause
│   └── team
├── sound
│   ├── countdown
│   ├── gameover
│   ├── gameover_shutout
│   ├── goal
│   ├── period_1
│   ├── period_2
│   ├── period_3
│   └── period_4
└── text
```
`pic` and `sound` subdirs should contain images. `text` should contain `.txt` files. Only `pause.txt` is supported for now.