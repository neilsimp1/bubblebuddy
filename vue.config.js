module.exports = {
	pluginOptions: {
		electronBuilder: {
			externals: ['onoff'],
			builderOptions: {
				linux: {
					target: 'dir',
					asar: true,
				}
			},
			nodeIntegration: true,
			mainProcessFile: 'src/main/main.ts',
			rendererProcessFile: 'src/app/app.ts',
		}
	}
}
